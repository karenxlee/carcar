import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="justifycontent-center navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        {/* onClick={() => window.location.reload(false)}  */}
        <div className="navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav flex-row flex-wrap navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
              <NavLink className="nav-link active" to="/manufacturers">Manufacturer</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/manufacturers/new">Create a Manufacturer</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/models">Models</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/models/new">Create a Model</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/automobiles">Automobiles</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/automobiles/create">Create an Automobile</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/salespeople">Salespeople</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/salespeople/create">Add a Salesperson</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/customers">Customers</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/customers/create">Add a Customer</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/sales">Sales</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/sales/create">Add a Sales</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/salesperson-history">Sales History</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/technicians">Technicians</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/technicians/new">Add a Technician</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/appointments">Service Appointments</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/appointments/new">Create a Service Appointment</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link active" to="/servicehistory">Service History</NavLink>
          </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
