import React, { useState, } from 'react';
import { useNavigate } from 'react-router-dom';
//define stateful variables using 'useState' hook
//useNavigate hook grabs reference to the navigation function: reroute after successful submission
function CustomerForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const navigate = useNavigate();
//handles form subissions. called when form is submitted, asyncronous. prevents default form submission behavior
// that normally handles everything automatically
    const handleSubmit = async (event) => {
        event.preventDefault();
//form data is sent to backend using POST method. if true, route to /customers using navigate() from react-router-dom
        try {
            const response = await fetch('http://localhost:8090/api/customers/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    first_name: firstName,
                    last_name: lastName,
                    address: address,
                    phone_number: phoneNumber
                }),
        });
        if (response.ok) {
            navigate('/customers');
          }
        } catch (error) {}
      };
//returns jsx to render form. values controlled by the useState hook
//'onChange' input for each field, event handler calls 'handleSubmit', triggering async requesto to the backend api and makes a new customer

      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a New Customer</h1>
              <form onSubmit={handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                  <input onChange={e => setFirstName(e.target.value)} value={firstName} placeholder="First Name" required type="text" id="first_name" className="form-control" />
                  <label htmlFor="first_name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={e => setLastName(e.target.value)} value={lastName} placeholder="Last Name" required type="text" id="last_name" className="form-control" />
                  <label htmlFor="last_name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={e => setAddress(e.target.value)} value={address} placeholder="Address" required type="text" id="address" className="form-control" />
                  <label htmlFor="address">Address</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={e => setPhoneNumber(e.target.value)} value={phoneNumber} placeholder="Phone Number" required type="tel" id="phone_number" className="form-control" />
                  <label htmlFor="phone_number">Phone Number</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }

    export default CustomerForm;
