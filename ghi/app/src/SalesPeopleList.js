import React, { useState, useEffect } from 'react';

function SalesPeopleList() {
  const [salesPeople, setSalesPeople] = useState([]);

  useEffect(() => {
    async function loadSalesPeople() {
        try {
          const response = await fetch('http://localhost:8090/api/salespeople/');
          if (!response.ok) throw new Error(`HTTP error! status: ${response.status}`);
          const data = await response.json();
          setSalesPeople(data.salespeople);
        } catch (error) {
          console.error(error);
        }
      }

    loadSalesPeople();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Employee ID</th>
          <th>First Name</th>
          <th>Last Name</th>
        </tr>
      </thead>
      <tbody>
        {salesPeople.map(salesPerson => (
          <tr key={salesPerson.id}>
            <td>{salesPerson.employee_id}</td>
            <td>{salesPerson.first_name}</td>
            <td>{salesPerson.last_name}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default SalesPeopleList;
