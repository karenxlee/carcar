// import React, { useEffect, useState } from 'react';

function TechnicianList(props) {
    if (props.technicians === undefined) {
        return null;
      }

    return(

    <div className="p-4 mt-4">
        <h1>Technician</h1>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
        </tr>
        </thead>
        <tbody>
        {props.technicians.map(technician => {
            return (
            <tr key={technician.id}>
                <td>{technician.employee_id}</td>
                <td>{technician.first_name}</td>
                <td>{technician.last_name}</td>
            </tr>
            );
        })}
        </tbody>
    </table>
    </div>

    );
}


export default TechnicianList;
