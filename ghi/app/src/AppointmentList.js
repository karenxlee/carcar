import React, { useEffect, useState } from 'react';

function AppointmentList(props) {

    const [appointments, setAppointments] = useState([]);
    // const [automobiles, setAutomobiles] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setAppointments(data.appointments.filter(appointment=> appointment.status==="ACTIVE"));
        }
    }

    const handleCancel = async (id) => {
    const appointmentUrl= `http://localhost:8080/api/appointments/${id}/cancel`;
        const fetchConfig = {
            method: "PUT",
            headers:{
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok){
            fetchData();
            window.location.reload(false);
        }

        }


    const handleFinish = async (id) => {
    const appointmentUrl= `http://localhost:8080/api/appointments/${id}/finish`;
        const fetchConfig = {
            method: "PUT",
            headers:{
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok){
            fetchData();
            window.location.reload(false);
        }

        }



    useEffect(()=> {
        fetchData();
    }, []);

    const formatDate = (dateTimeString) => {
        const dateTime = new Date(dateTimeString);
        const options = {
            timeZone: "America/Los_Angeles",
        }
        const date = dateTime.toLocaleDateString("en-US", options);
        const time = dateTime.toLocaleTimeString("en-US", options);
        return {date, time};
    }


    const checkVip = (appointmentVin) => {
        const matchAutomobile = props.automobiles.filter(
            automobile => automobile.vin === appointmentVin);
            //empty brackets in console.log means func is filtering
            // console.log(matchAutomobile);
            return matchAutomobile.length? "Yes" : "No";
            //if matchautomobile length it true? return yes if >0 or not 0, if not, return no
    };

    return(
        <div className="p-4 mt-4">
        <h1>Service Appointments</h1>
        <table className="table table-striped">
        <thead>
        <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer Name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>

        </tr>
        </thead>
        <tbody>
        {appointments.map((appointment) => {
            const {date, time} = formatDate(appointment.date_time);
            return (
            <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{checkVip(appointment.vin)}</td>
                <td>{appointment.customer}</td>
                <td>{date}</td>
                <td>{time}</td>
                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                <td>{appointment.reason}</td>
                <td>
                    <button onClick={()=> handleCancel(appointment.id)} className="btn btn-primary">Cancel</button>
                </td>
                <td>
                    <button onClick={()=> handleFinish(appointment.id)} className="btn btn-primary">Finish</button>
                </td>
            </tr>
            );
        })}
        </tbody>
    </table>
    </div>

    );
}

export default AppointmentList;

