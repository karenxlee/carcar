import React, { useEffect, useState } from 'react';

function SalesList() {
  const [sales, setSales] = useState([]);

  useEffect(() => {
    const fetchSales = async () => {
      const response = await fetch('http://localhost:8090/api/sales');
      const data = await response.json();

      if (!data.sales || !Array.isArray(data.sales)) {
        console.error('Invalid sales data:', data);
        return;
      }

      const salesWithDetails = await Promise.all(data.sales.map(async (sale) => {
        if (!sale || typeof sale !== 'object') {
          console.error('Invalid sale:', sale);
          return null;
        }

        const salespersonResponse = await fetch(`http://localhost:8090/api/salespeople/${sale.salesperson.id}`);
        const salespersonData = await salespersonResponse.json();

        const customerResponse = await fetch(`http://localhost:8090/api/customers/${sale.customer.id}`);
        const customerData = await customerResponse.json();

        return {
          ...sale,
          salespersonEmployeeId: salespersonData.employee_id,
          salespersonName: `${salespersonData.first_name} ${salespersonData.last_name}`,
          customer: customerData,
          automobileVin: sale.automobile.vin,
          price: sale.price,
        };
      }));

      setSales(salesWithDetails);
    };

    fetchSales();
  }, []);

  return (
    <div>
      <h1>Sales</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson Employee ID</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale, index) => (
            <tr key={index}>
              <td>{sale.salespersonEmployeeId}</td>
              <td>{sale.salespersonName}</td>
              <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
              <td>{sale.automobileVin}</td>
              <td>{sale.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalesList;
