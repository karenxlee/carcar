import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import VehicleModelForm from './VehicleModelForm';
import VehicleModelList from './VehicleModelList';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import SalesPeopleList from './SalesPeopleList';
import SalesPeopleForm from './SalesPeopleForm';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalesList from './SalesList';
import SaleForm from './SaleForm';
import SalesPeopleHistory from './SalesPeopleHistory';
//-Dakota: I imported and logged my routes, they should be working
function App(props) {
  const [automobiles, setAutomobiles] = useState([]);

  async function loadAutomobiles() {
    try {
      const response = await fetch('http://localhost:8100/api/automobiles/');
      if (response.ok) {
        const data = await response.json();
        setAutomobiles(data.autos);
      } else {
        console.error('Error fetching automobiles:', response.status, response.statusText);
      }
    } catch (error) {
      console.error('Error fetching automobiles:', error);
    }
  }

  // Load automobiles on component mount
  useEffect(() => {
    loadAutomobiles();
  }, []);

  // Function to handle form submission for creating a new automobile
  async function handleAutomobileFormSubmit(formData) {
    try {
      const response = await fetch('http://localhost:8100/api/automobiles/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });

      if (response.ok) {
        // Reload automobiles after successful creation
        loadAutomobiles();
        console.log('Automobile created successfully!');
      } else {
        console.error('Error creating automobile:', response.status, response.statusText);
      }
    } catch (error) {
      console.error('Error creating automobile:', error);
    }
  }


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians" element={<TechnicianList technicians= {props.technicians}/>} />
          <Route path="technicians/new" element={<TechnicianForm />} />
          <Route path="appointments" element={<AppointmentList appointments={props.appointments} automobiles={automobiles}/>} />
          <Route path="appointments/new" element={<AppointmentForm />} />
          <Route path="servicehistory" element={<ServiceHistory appointments={props.appointments} automobiles={automobiles}/>} />
          <Route path="manufacturers/new" element={<ManufacturerForm />} />
          <Route path="manufacturers" element={<ManufacturerList manufacturers= {props.manufacturers}/>} />
          <Route path="models/new" element={<VehicleModelForm />} />
          <Route path="models" element={<VehicleModelList models= {props.models}/>} />
          <Route
            path="/automobiles/create"
            element={<AutomobileForm onSubmit={handleAutomobileFormSubmit} />}
          />
          <Route
            path="/automobiles"
            element={<AutomobileList automobiles={automobiles} />}
          />
          <Route
            path="/salespeople"
            element={<SalesPeopleList />}
          />
          <Route
            path="/salespeople/create"
            element={<SalesPeopleForm />}
          />
          <Route
            path="/customers"
            element={<CustomerList />}
          />
          <Route
            path="/customers/create"
            element={<CustomerForm />}
          />
          <Route
            path="/sales"
            element={<SalesList />}
          />
          <Route
            path="/sales/create"
            element={<SaleForm />}
            />
            <Route
            path="/salesperson-history"
            element={<SalesPeopleHistory />}
            />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
