import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
//putting all functions into a load data
const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadData() {

  const techniciansResponse = await fetch ('http://localhost:8080/api/technicians/');
  const appointmentsResponse = await fetch ('http://localhost:8080/api/appointments/');
  const manufacturersResponse = await fetch ('http://localhost:8100/api/manufacturers/');
  const modelsResponse = await fetch('http://localhost:8100/api/models/');


  if (techniciansResponse.ok && appointmentsResponse.ok &&
    manufacturersResponse.ok && modelsResponse.ok) {
    const techniciansData= await techniciansResponse.json();
    const appointmentsData = await appointmentsResponse.json();
    const manufacturersData = await manufacturersResponse.json();
    const modelsData = await modelsResponse.json();

    root.render(
      <React.StrictMode>
        <App technicians={techniciansData.technicians}
        appointments={appointmentsData.appointments}
        manufacturers={manufacturersData.manufacturers}
        models= {modelsData.models}
        />
      </React.StrictMode>
    );
  } else {
    console.log('error fetching data');
  }
}

loadData();

// async function loadTechnicians() {
//   const response = await fetch ('http://localhost:8080/api/technicians/');
//   if (response.ok) {
//     const data = await response.json();
//     root.render(
//       <React.StrictMode>
//         <App technicians={data.technicians}/>
//       </React.StrictMode>
//     );
//   } else {
//     console.log(response)
//   }
// }
// loadTechnicians();


// async function loadAppointments() {
//   const response = await fetch ('http://localhost:8080/api/appointments/');
//   if (response.ok) {
//     const data = await response.json();
//     root.render(
//       <React.StrictMode>
//         <App appointments={data.appointments}/>
//       </React.StrictMode>
//     );
//   } else {
//     console.log(response)
//   }
// }
// loadAppointments();

