import React, { useState } from 'react';
//? = if it's not null use the filteredappointments, if it's null use unfilteredappts
// if (filtered appoints has stuff) {
//     use filtered appouints
//     }
//     else {
//     use the full list
//     }
function filterAppointments(filteredAppointments, unfilteredAppointments, props){
    const appointmentsToMap= filteredAppointments.length?filteredAppointments:unfilteredAppointments
    // console.log("appt filtered: ", filteredAppointments);
    const formatDate = (dateTimeString) => {
        const dateTime = new Date(dateTimeString);
        const options = {
            timeZone: "America/Los_Angeles",
        }
        const date = dateTime.toLocaleDateString("en-US", options);
        const time = dateTime.toLocaleTimeString("en-US", options);
        return {date, time};
    }

    const checkVip = (appointmentVin, props) => {
        const matchAutomobile = props.automobiles.filter(
            automobile => automobile.vin === appointmentVin);
            //empty brackets in console.log means func is filtering
            // console.log(matchAutomobile);
            return matchAutomobile.length? "Yes" : "No";
            //if matchautomobile it true? return yes if >0 or not 0, if not, return no
    };
    // console.log("vip checked", checkVip);
    // console.log("serach completed: ", appointmentsToMap);
    return(

            appointmentsToMap.map((appointment) => {
            const {date, time} = formatDate(appointment.date_time);
            return (
            <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{checkVip(appointment.vin, props)}</td>
                <td>{appointment.customer}</td>
                <td>{date}</td>
                <td>{time}</td>
                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
            </tr>
            );
        }))
}

function ServiceHistory(props) {

    const[searchInput, setSearchInput] = useState('');
    const[filteredAppointments, setFilteredAppointments] = useState([]);

    const handleSearchInputChange = (event) => {
      const value = event.target.value;
      setSearchInput(value);
    }

    const handleSearch = (event) => {
      event.preventDefault();
    //   console.log("now in search handle input is: ", searchInput);
      const filteredAppointments = props.appointments.filter(appointment =>
        appointment.vin.toLowerCase().includes(searchInput.toLowerCase())
      );
    //   console.log("props automobiles: ", props.automobiles);

    //   console.log("search handle: ",filteredAppointments);
      setFilteredAppointments(filteredAppointments);
    }

    if (props.appointments === undefined) {
        return null;
      }



    return(
        <div className="p-4 mt-4">
        <h1>Service History</h1>
        <form onSubmit={handleSearch} >
        <div className="flex" style={{display: 'flex', justifyContent: 'flex-end'}}>
        <input className="form-control" type="text" placeholder="Search by VIN..."
        onChange={handleSearchInputChange} value={searchInput}/>
        <button type="submit" className="btn btn-primary col-1">Search</button>
        </div>
        </form>
        <table className="table table-striped">
        <thead>
        <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer Name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>

        </tr>
        </thead>
        <tbody>
            {filterAppointments(filteredAppointments,props.appointments,props)}
        </tbody>
    </table>
    </div>

    );
}

export default ServiceHistory;


