//takes props as an argument, passed in from parent component
function AutomobileList(props) {
//checks to see if 'props.automobiles' is falsy. if so return 'null'. stops errors when data is unavail
    if(!props.automobiles) {
        return null;
    }
    //render table if data avail
    //add stripe for flair with bootstrap
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Vin</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                </tr>
            </thead>
            <tbody>
                {props.automobiles.map(automobile => (
                    <tr key={automobile.href}>
                        <td>{automobile.vin}</td>
                        <td>{automobile.color}</td>
                        <td>{automobile.year}</td>
                        <td>{automobile.model.name}</td>
                        <td>{automobile.model.manufacturer.name}</td>
                        <td>{automobile.sold ? 'Yes' : 'No'}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default AutomobileList;
//use map fn to iterate over every element in props.automobiles list
