function VehicleModelList(props) {
    if (props.models === undefined) {
        return null;
      }

    return(

    <div className="p-4 mt-4">
        <h1>Models</h1>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Manufacturuer</th>
            <th>Picture</th>
        </tr>
        </thead>
        <tbody>
        {props.models.map(model => {
            return (
            <tr key={model.id}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td>
                    <img style={{ width:300, height:200}}
                    src={model.picture_url} alt="new picture"
                    />
                </td>
            </tr>
            );
        })}
        </tbody>
    </table>
    </div>

    );
}


export default VehicleModelList;
