import React, { useState, useEffect } from 'react';

function SalesPeopleHistory() {
  const [salesPeople, setSalesPeople] = useState([]);
  const [selectedSalesPerson, setSelectedSalesPerson] = useState('');
  const [salesHistory, setSalesHistory] = useState([]);

  useEffect(() => {
    async function loadSalesPeople() {
        try {
          const response = await fetch('http://localhost:8090/api/salespeople/');
          if (!response.ok) throw new Error(`HTTP error! status: ${response.status}`);
          const data = await response.json();
          setSalesPeople(data.salespeople);
        } catch (error) {
          console.error(error);
        }
      }

    loadSalesPeople();
  }, []);

  useEffect(() => {
    if (selectedSalesPerson !== '') {
      async function loadSalesHistory() {
        try {
          const response = await fetch(`http://localhost:8090/api/salespeople/${selectedSalesPerson}/sales`);
          if (!response.ok) throw new Error(`HTTP error! status: ${response.status}`);
          const data = await response.json();
          setSalesHistory(data.sales);
        } catch (error) {
          console.error(error);
        }
      }

      loadSalesHistory();
    }
  }, [selectedSalesPerson]);

  const handleChange = (event) => {
    setSelectedSalesPerson(event.target.value);
  }

  return (
    <div>
      <select value={selectedSalesPerson} onChange={handleChange}>
        <option value="">Select a salesperson</option>
        {salesPeople.map(salesPerson => (
          <option key={salesPerson.employee_id} value={salesPerson.employee_id}>
            {salesPerson.first_name} {salesPerson.last_name}
          </option>
        ))}
      </select>

      {salesHistory.length > 0 && (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Salesperson</th>
              <th>Customer</th>
              <th>Automobile VIN</th>
              <th>Price of Sale</th>
            </tr>
          </thead>
          <tbody>
            {salesHistory.map(sale => (
              <tr key={sale.id}>
                <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
                <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
}

export default SalesPeopleHistory;
