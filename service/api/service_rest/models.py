from django.db import models


# Create your models here.
class Status(models.Model):

    id = models.PositiveSmallIntegerField(primary_key=True)
    name = models.CharField(max_length=10,unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("id",)
        verbose_name_plural = "statuses"

class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.first_name
        # (+last_name?)

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)

class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.ForeignKey(
        Status,
        related_name = "appointments",
        on_delete=models.PROTECT,
    )

    vin = models.CharField(max_length=100)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name = "names",
        on_delete=models.PROTECT,
    )

    def cancel(self):
        status = Status.objects.get(name="CANCEL")
        self.status = status
        self.save()

    def finish(self):
        status = Status.objects.get(name="FINISH")
        self.status = status
        self.save()

    def __str__(self):
        return self.customer
