# Generated by Django 4.0.3 on 2023-07-26 23:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0005_alter_status_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='status',
            name='id',
            field=models.PositiveSmallIntegerField(primary_key=True, serialize=False),
        ),
    ]
