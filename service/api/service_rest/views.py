from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment, Status

# Create your views here.

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["id",
                  "first_name",
                  "last_name",
                  "employee_id",
                  ]

class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = ["id",
                  "first_name",
                  "last_name",
                  "employee_id",
                  ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["id",
                  "date_time",
                  "status",
                  "reason",
                  "customer",
                  "vin",
                  "technician",
              ]
    def get_extra_data(self, o):
        return {"status": o.status.name}

    encoders = {
        "technician": TechnicianListEncoder(),
}



class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = ["id",
                  "date_time",
                  "reason",
                  "status",
                  "vin",
                  "customer",
                  "technician",]

    def get_extra_data(self, o):
        return {"status": o.status.name}

    encoders = {
        "technician": TechnicianDetailEncoder(),
}



@require_http_methods (["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        try:
            technicians = Technician.objects.all()
            return JsonResponse(
                {"technicians": technicians},
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except (ValueError, KeyError) as err:
            return JsonResponse(
                {"message": str(err)},
                status=400,
            )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)

            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except (ValueError, KeyError) as err:
            return JsonResponse(
                {"message": str(err)},
                status=404,
            )


@require_http_methods (["DELETE"])
def delete_technicians(request,pk):
    if request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count>0})



@require_http_methods(["GET","POST",])
def list_appts(request, id=None):

    if request.method == "GET":
        if id is not None:
            appointments = Appointment.objects.filter(technician=id)
        else:
            appointments = Appointment.objects.all()

        return JsonResponse(
            {"appointments":appointments},
            encoder=AppointmentListEncoder,
            #maybe can put detailencoder to try instead
        )
    else:
        content = json.loads(request.body)
        try:
            technician_href = content["technician"]
            technician = Technician.objects.get(id=technician_href)
            content["technician"] = technician

            content["status"] = content.get("status", "ACTIVE")
            status = Status.objects.get(name=content["status"])
            content["status"] = status

        except Technician.DoesNotExist and Status.DoesNotExist:
            return JsonResponse(
                {"message":"invalid technician id or status name"},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods (["DELETE"])
def delete_appts(request,pk):
    if request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count>0})


@require_http_methods(["PUT"])
def cancel_appts(request,pk):
    appointment = Appointment.objects.get(id=pk)
    appointment.cancel()
    return JsonResponse(
        appointment,
        encoder=AppointmentDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def finish_appts(request,pk):
    appointment = Appointment.objects.get(id=pk)
    appointment.finish()
    return JsonResponse(
        appointment,
        encoder=AppointmentDetailEncoder,
        safe=False,
    )
