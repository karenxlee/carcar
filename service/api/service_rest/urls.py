from django.urls import path
from .views import list_technicians, delete_technicians, list_appts, delete_appts, cancel_appts, finish_appts


urlpatterns = [
    path("technicians/", list_technicians, name="list_technicians"),
    path("technicians/{technician.id}/", delete_technicians, name="delete_technicians"),
    path("appointments/", list_appts, name="list_appts"),
    path("appointments/<int:pk>/", delete_appts, name="delete_appts"),
    path("appointments/<int:pk>/cancel", cancel_appts, name="cancel_appts"),
    path("appointments/<int:pk>/finish", finish_appts, name="finish_appts"),

]
