# CarCar
CarCar is an application for managing the aspects of an automobile dealership. It manages the inventory, automobile sales, and automobile services.
Team:

* Karen - Automobile Service
* Dekota - Automobile Sales


## Getting Started

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone <<https://gitlab.com/DakotaSteppe/project-beta.git>>

3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/


## Design

CarCar is made up of 3 microservices which interact with one another.

- **Inventory**
- **Services**
- **Sales**

Here is a screenshot of the CarCar web application and diagram:

https://excalidraw.com/#room=aa28204015ce048d3ed9,AANtU45QMynChbR7tl5d5g


## Integration - How we put the "team" in "team"

Our Inventory and Sales domains work together with our Service domain to make everything here at **CarCar** possible.

It starts from our inventory domain. We keep a record of automobiles on our lot that are available to buy. Our sales and service microservices obtain information from the inventory domain, using a **poller**, which talks to the inventory domain to keep track of which vehicles we have in our inventory so that the service and sales team always has up-to-date information.


## Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser

### Manufacturers:


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/:id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/:id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/:id/


JSON body to send data:

Create and Update a manufacturer (SEND THIS JSON BODY):
- You cannot make two manufacturers with the same name
```
{
  "name": "Chrysler"
}
```
The return value of creating, viewing, updating a single manufacturer:
```
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Chrysler"
}
```
Getting a list of manufacturers return value:
```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}
```

### Vehicle Models:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/:id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/:id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/:id/

Create and update a vehicle model (SEND THIS JSON BODY):
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
  "manufacturer_id": 1
}
```

Updating a vehicle model can take the name and/or picture URL:
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
}
```
Return value of creating or updating a vehicle model:
- This returns the manufacturer's information as well
```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```
Getting a List of Vehicle Models Return Value:
```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "image.yourpictureurl.com",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```

### Automobiles:
- The **'vin'** at the end of the detail urls represents the VIN for the specific automobile you want to access. This is not an integer ID. This is a string value so you can use numbers and/or letters.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/:vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/:vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/:vin/


Create an automobile (SEND THIS JSON BODY):
- You cannot make two automobiles with the same vin
```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```
Return Value of Creating an Automobile:
```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "777",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "R8",
		"picture_url": "image.yourpictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	}
}
```
To get the details of a specific automobile, you can query by its VIN:
example url: http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/

Return Value:
```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "green",
  "year": 2011,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "image.yourpictureurl.com",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  }
}
```
You can update the color and/or year of an automobile (SEND THIS JSON BODY):
```
{
  "color": "red",
  "year": 2012
}
```
Getting a list of Automobile Return Value:
```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "image.yourpictureurl.com",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }
  ]
}
```
## Service microservice

Hello! The service microservice is an extension of the dealership that provides service repairs for your vehicle.

As automobiles are purchased, we keep track of the vin number of each automobile and you are able to receive the special treament of being a VIP!

Being a VIP, you will receive oil changes for life, a complimentary neck massage, foot massage, and free baverages while in the waiting room, which comes with a basic car grooming services whenever you revisit us!

This area is going to be broken down into the various API endpoints for service along with the format needed to send data to each component.
The basics of service are as follows:
1. Our super nice and helpful technician staff
2. Service Appointments


### Status - To provide you the status of the Service
VERY IMPORTANT!

First of all, please go to docker containters under project-beta and click inside the service-api-1 container's terminal and run the following commands:

python manage.py createsuperuser

(now create your username and password)

Then on your broswer, navigate to django admin site http://localhost:8080/admin/ to log in and click on the Statuses folder and then click "ADD STATUS +" button, add:

id:1
Name: CANCEL

id:2
Name: FINISH

id:3
Name: ACTIVE

then exit django admin site to continue the following steps.


### Technicians - The heart of what we do here at CarCar

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:pk>/


LIST TECHNICIANS: Following this endpoint will give you a list of all technicians that are currently employed.
Since this is a GET request, you do not need to provide any data.
```
Example:
{
	"technicians": [
		{
			"id": 1,
			"first_name": "Sam",
			"last_name": "Smith",
			"employee_id": "079283"
		}
	]
}
```

CREATE TECHNICIAN - What if we hired a new technician (In this economy even)? To create a technician, you would use the following format to input the data and you would just submit this as a POST request.
```
{
	"id": 2,
	"first_name": "Batman",
	"last_name": "Isbad",
	"employee_id": "777778"
}


```
As you can see, the data has the same format. In this example, we just changed the "first_name" field from "Sam" to "Batman", "last_name" field from "Smith" to "Isbad". We also assigned him the "employee_id" value of "777778" instead of "079283" and the "id" value of 2 instead of 1.
Once we have the data into your request, we just hit "Send" and it will create the technician "Batman". To verify that it worked, just select follow the "LIST TECHNICIAN" step from above to show all technicians.
With any luck, both Sam and Batman will be there.
Here is what you should see if you select "LIST TECHNICIAN" after you "CREATE TECHNICIAN" with Batman added in:
```
{
	"technicians": [
		{
			"id": 1,
			"first_name": "Sam",
			"last_name": "Smith",
			"employee_id": "079283"
		},
		{
			"id": 2,
			"first_name": "Batman",
			"last_name": "Isbad",
			"employee_id": "777778"
		},
	]
}

DELETE TECHNICIAN - If we decide to "go another direction" as my first boss told me, then we need to remove the technician from the system. To do this, you just need to change the request type to "DELETE" instead of "POST". You also need to pull the "id" value to make sure you delete the correct one. Once they are "promoted to customer" they will no longer be in our page that lists
all technicians.


And that's it! You can view all technicians, look at the details of each technician, and create technicians.
Just follow the steps in CREATE TECHNICIAN and the "id" field will be populated for you.
If you get an error, make sure your server is running and that you are feeding it in the data that it is requesting.
If you feed in the following:
```
{
	"name": "Sam",
	"id": 1,
	"favorite_food": "Tacos"
}

You will get an error because the system doesn't know what what to do with "Tacos" because we aren't ever asking for that data. We can only send in data that Json is expecting or else it will get angry at us.

```

### Service Appointments: We'll keep you on the road and out of our waiting room

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List service appointments | GET | http://localhost:8080/api/appointments/
| Create service appointment | POST | http://localhost:8080/api/appointments/
| Delete service appointment | DELETE | http://localhost:8080/api/appointments/<int:pk>/
| Change service appointment to "CANCEL" | PUT | http://localhost:8080/api/appointments/<int:pk>/cancel
| Change service appointment to "FINISH" | PUT | http://localhost:8080/api/appointments/<int:pk>/finish



LIST SERVICE APPOINTMENT: This will return a list of all current service appointment.
This is the format that will be displayed.
Spoiler alert! Remember, the way that it is returned to you is the way that the data needs to be accepted. Remember, the "id" is automatically generated, so you don't need to input that.
Also, the "date" and "time" field HAS TO BE IN THIS FORMAT
```
{
	"appointments": [
		{
			"id": 2,
			"date_time": "2023-07-29T18:00:00+00:00",
			"status": "CANCEL",
			"reason": "broken engine",
			"customer": "Tammy",
			"vin": "3FAFP13P41R199033",
			"technician": {
				"id": 3,
				"first_name": "Hello",
				"last_name": "Daniel",
				"employee_id": "999888"
			}
		},
		{
			"id": 1,
			"date_time": "2023-07-29T21:50:00+00:00",
			"status": "CANCEL",
			"reason": "broken trunk",
			"customer": "Barbie Girl",
			"vin": "1ZVFT80N475211367",
			"technician": {
				"id": 2,
				"first_name": "Teddy",
				"last_name": "Bear",
				"employee_id": "098765"
			}
		},
		{
			"id": 3,
			"date_time": "2023-07-30T00:00:00+00:00",
			"status": "ACTIVE",
			"reason": "broken window",
			"customer": "Alice Border",
			"vin": "1C3CC5FB2AN120174",
			"technician": {
				"id": 2,
				"first_name": "Teddy",
				"last_name": "Bear",
				"employee_id": "098765"
			}
		}
	]
}
```

CREATE SERVICE APPOINTMENT - This will create a service appointment with the data input. It must follow the format. Remember, the "id" is automatically generated, so don't fill that in. To verify
that it was added, just look at your service appointment list after creating a service appointment and it should be there.
```
{
	"id": 6,
	"date_time": "2023-07-30",
	"reason": "broken window",
	"vin": "1C3CC5FB2AN120174",
	"customer": "Alice Border",
	"technician": 2
}



DELETE SERVICE APPOINTMENT - Just input the "id" of the service appointment that you want to delete at the end of the url. For example, if we wanted to delete the above service history appointment for Barry
because we accidently input his name as "Gary", we would just enter 'http://localhost:8080/api/appointments/6' into the field and send the request. We will receive a confirmation message saying that
the service appointment was deleted as "deleted" = true.


CHANGE SERVICE APPOINTMENT TO "CANCEL" - Just input the "id" of the service appointment that you want to cancel at the end of the url then add "/cancel" so it will change the status of that specific appointment to cancel. For example, you want appointment id 1 to change its status to cancel, you will do "http://localhost:8080/api/appointments/1/cancel" do change its status.


CHANGE SERVICE APPOINTMENT TO "FINISH" - Just input the "id" of the service appointment that you want to finsih at the end of the url then add "/finish" so it will change the status of that specific appointment to finish. For example, you want appointment id 1 to change its status to finish, you will do "http://localhost:8080/api/appointments/1/finish" do change its status.

Action
Method
URL

Sales API LocalHost 8090 information

-To Interact with the SalesPerson Model:
Action
Method
URL

List salespeople
GET
http://localhost:8090/api/salespeople/
Create a salesperson
POST
http://localhost:8090/api/salespeople/
Delete a specific salesperson
DELETE
http://localhost:8090/api/salespeople/:id
------API Information--------
The list of Salespeople will return the id, first name, last name and employee id:
{
    "salespeople": [
        {
            "id": 1,
            "first_name": "Blake",
            "last_name": "B",
            "employee_id": "BB123"
        }
    ]
}

To create a salesperson your body should look like this and include the first name, last name and employee id in the JSON body:
{
  "first_name": "Blake",
  "last_name": "B",
  "employee_id": "BB123"
}

The delete saleperson will return True after entering the salespeople ID at the end of the URL
{
    "deleted": true
}

-To Interact with the Customer Model:
Action
Method
URL

List customers
GET
http://localhost:8090/api/customers/
Create a customer
POST
http://localhost:8090/api/customers/
Delete a specific customer
DELETE
http://localhost:8090/api/customers/:id

------API Information--------
Listing customers will show you the ID, first name, last name, address and phone number like this:
{
    "customers": [
        {
            "id": 1,
            "first_name": "Dak",
            "last_name": "Ste",
            "address": "8080 Street",
            "phone_number": "123-321-1234"
        }
    ]
}

To create a customer you need the first name, last name, address and phone number. Your JSON body should look like this:
{
    "first_name": "Dak",
    "last_name": "Ste",
    "address": "8080 Street",
    "phone_number": "123-321-1234",
    "id": 1
}

A deleted Customer will show True like below:
{
    "deleted": true
}

-To Interact with the Sales Model:
Action
Method
URL

List sales
GET
http://localhost:8090/api/sales/
Create a sale
POST
http://localhost:8090/api/sales/
Delete a sale
DELETE
http://localhost:8090/api/sales/:id
To Show Sales History
GET
http://localhost:8090/api/salespeople/BB123/sales
To show an individual sale
GET
http://localhost:8090/api/sales/id/
The sales list will show you the automobile, salesperson, customer and price with their included attributes like below:
{
    "sales": [
        {
            "automobile": {
                "vin": "767678",
                "sold": true,
                "id": 1
            },
            "salesperson": {
                "first_name": "Blake",
                "last_name": "B",
                "employee_id": "BB123",
                "id": 1
            },
            "customer": {
                "first_name": "Dak",
                "last_name": "Ste",
                "address": "8080 Street",
                "phone_number": "123-321-1234",
                "id": 1
            },
            "price": 40000,
            "id": 3
        }
    ]
}

To create a sale you will need automobile (VIN), salesperson(employee ID), customer (id), and price which will look similiar to this:
{
    "automobile": "767678",
    "salesperson": "BB123",
    "customer": 1,
    "price": 40000
}

Deleting a sale you will need the sale ID in the URL and you will get deleted = True like this:
{
    "deleted": true
}

You can also show an individual sale with the sale/id in a GET request for the URL and you will get the automobile, saleperson, customer and price similiar to this:
{
    "automobile": {
        "vin": "767678",
        "sold": true,
        "id": 1
    },
    "salesperson": {
        "first_name": "Blake",
        "last_name": "B",
        "employee_id": "BB123",
        "id": 1
    },
    "customer": {
        "first_name": "Dak",
        "last_name": "Ste",
        "address": "8080 Street",
        "phone_number": "123-321-1234",
        "id": 1
    },
    "price": 40000,
    "id": 3
}

For my front-end website to show a history of sales by a certain salesperson in the dropdown menu I needed to add an extra view for saleshistory. To do this you will need to include the employee's ID in the URL and this will return something similiar to this:
{
    "sales": [
        {
            "automobile": {
                "vin": "767678",
                "sold": true,
                "id": 1
            },
            "salesperson": {
                "first_name": "Blake",
                "last_name": "B",
                "employee_id": "BB123",
                "id": 1
            },
            "customer": {
                "first_name": "Dak",
                "last_name": "Ste",
                "address": "8080 Street",
                "phone_number": "123-321-1234",
                "id": 1
            },
            "price": 40000,
            "id": 3
        },
        {
            "automobile": {
                "vin": "12345678",
                "sold": true,
                "id": 2
            },
            "salesperson": {
                "first_name": "Blake",
                "last_name": "B",
                "employee_id": "BB123",
                "id": 1
            },
            "customer": {
                "first_name": "Dak",
                "last_name": "Ste",
                "address": "8080 Street",
                "phone_number": "123-321-1234",
                "id": 1
            },
            "price": 50000,
            "id": 4
        },
        {
            "automobile": {
                "vin": "55555555555",
                "sold": true,
                "id": 3
            },
            "salesperson": {
                "first_name": "Blake",
                "last_name": "B",
                "employee_id": "BB123",
                "id": 1
            },
            "customer": {
                "first_name": "Dak",
                "last_name": "Ste",
                "address": "8080 Street",
                "phone_number": "123-321-1234",
                "id": 1
            },
            "price": 800000,
            "id": 5
        }
    ]
}

-To Interact with the AutomobileVO Model:
Action
Method
URL

List AutomobileVO
GET
http://localhost:8090/api/automobiles/
If you need to check if the poller works, you can actually check your automobileVO. It will show you the cars from automobiles inventory and whether it's been sold or not
{
    "automobiles": [
        {
            "id": 1,
            "vin": "767678",
            "sold": true
        },
        {
            "id": 2,
            "vin": "12345678",
            "sold": true
        }
    ]
}


