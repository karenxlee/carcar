from django.urls import path
from .views import (
    list_salespeople,
    show_salesperson,
    list_customers,
    show_customer,
    list_sales,
    show_sale,
    list_automobilevo,
    salesperson_sales,
)

urlpatterns = [
    path('salespeople/', list_salespeople, name='list_salespeople'),
    path("salespeople/<int:pk>/", show_salesperson, name="show_salesperson"),
    path("salespeople/<str:employee_id>/sales", salesperson_sales, name="salesperson_sales"),
    path('customers/', list_customers, name='list_customers'),
    path("customers/<int:pk>/", show_customer, name="show_customer"),
    path('sales/', list_sales, name='list_sales'),
    path('sales/<int:pk>/', show_sale, name='show_sale'),
    path('automobiles/', list_automobilevo, name='list_automobilevo'),
]
