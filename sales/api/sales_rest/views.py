import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Salesperson, Customer, AutomobileVO, Sale
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import FieldError
from django.db import transaction
from django.shortcuts import get_object_or_404

#Encoders Here
#------------------------------------------------------------------------------------------
class SalespersonEncoder(ModelEncoder):
    def default(self, obj):
        if isinstance(obj, Salesperson):
            return {
                "first_name": obj.first_name,
                "last_name": obj.last_name,
                "employee_id": obj.employee_id
            }
        return super().default(obj)

#coversions for Views, extension of ModelEncoders and serializes

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]


class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id", "id"]


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold", "id"]

class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = ["automobile", "salesperson", "customer", "price", "id"]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "salesperson": SalespersonDetailEncoder(),
        "customer": CustomerDetailEncoder()
    }

#def default(self, obj) overrides the parent's default method *ModelEncoder
#if instance checks if obj is an instance of Sale
#custom encoders for Foreign Keys in the 'Sale' model
#  let's each encoder do it's respective conversions
# return super().default(obj) will call Model Encoder default classes to handle
#  serialization if the obj isn't an instance of 'Sale'
    def default(self, obj):
        if isinstance(obj, Sale):
            return {
                "automobile": AutomobileVODetailEncoder().default(obj.automobile),
                "salesperson": SalespersonDetailEncoder().default(obj.salesperson),
                "customer": CustomerDetailEncoder().default(obj.customer),
                "price": obj.price,
                "id": obj.id
            }
        return super().default(obj)

#custom encoders for Foreign Keys in the 'Sale' model
#  let's each encoder do it's respective conversions
class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = ["automobile", "salesperson", "customer", "price", "id"]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "salesperson": SalespersonDetailEncoder(),
        "customer": CustomerDetailEncoder()
    }
#----------------------------------------------------------------------------------------------------------------
#Request Handlers: what they do, how they operate!

#requre_http_methods decorator lets only GET & POST requests through
#Get: grabs all 'Salesperson' objs from database using 'Salesperson.objects.all()', converts them to a dictionary
#  list so it's json compatible with 'list(salespeople.values())', then returns the json list of salespeople.
#POST: expects JSON data in the body like in insomnia, loads that data w/ 'json.loads(request.body)', then creates
#  new 'Salesperson' obj in database with 'Salesperson.objects.create(**content)'. Finally it returns a JSON response
#  with the newly created salesperson using 'JsonResponse'.
#Safe=False basically just makes JSON accept {dictionaries}
@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": list(salespeople.values())},
            safe=False
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )

#Filters so only GET,DELETE,PUT can have access
#takes request and the primary key(id) in this case since django makes it
#DELETE: if pk matches then deletes the obj and sends back message of confirmation
#PUT: if pk matches, then load the new json body request
#  loop over the k,v and update the values for each attribute
#  then saves changes to database
@require_http_methods(["DELETE", "GET", "PUT"])
def show_salesperson(request, pk):
    try:
        salesperson = Salesperson.objects.get(id=pk)
    except Salesperson.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid salesperson id!"},
            status=400,
        )

    if request.method == "GET":
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        salesperson.delete()
        return JsonResponse({"deleted": True})
    elif request.method == "PUT":
        content = json.loads(request.body)
        for key, value in content.items():
            if hasattr(salesperson, key):
                setattr(salesperson, key, value)
        salesperson.save()
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": list(customers.values())},
            encoder=CustomerListEncoder
        )
    else:  # POST
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def show_customer(request, pk):
    try:
        customer = Customer.objects.get(id=pk)
    except Customer.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid customer id!"},
            status=400,
        )

    if request.method == "GET":
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        customer.delete()
        return JsonResponse({"deleted": True})
    elif request.method == "PUT":
        content = json.loads(request.body)
        for key, value in content.items():
            if hasattr(customer, key):
                setattr(customer, key, value)
        customer.save()
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )

#Filters so only GET,POST methods reach view
#GET: grabs all 'Sale' obj's from the database and turn them in to a dict so JSON can read
#  returns the list of sales.
#sales_list creates a list of dictionaries for each sale obj. Uses SaleListEncoder to turn the
#  objs into a dict
#POST: needs all fields to be filled or throws an error 400(invalid request) which is usually bad
#syntax, or missing info.
# Tries to grab a automobile value object using the vin
# checks to see if the automobile VO has been flagged as 'sold'
# Tries to get Salesperson obj from database ising the employee ID get(employee_id=salesperson_id)
# Tries to grab customer obj from database using .customer.objects.get(id=customer_id)
# transaction.atomic() runs an all or nothing to keep data consistant. *can be inefficient when traffic increases
#creates a new sale obj in the database with the provided info
#'automobile.sold = True' updates the sold status of the vehicle to show it's status
@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        sales_list = [SaleListEncoder().default(sale) for sale in sales]
        return JsonResponse({"sales": sales_list}, safe=False)

    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]
            salesperson_id = content["salesperson"]
            customer_id = content["customer"]
            price = content["price"]
        except KeyError as e:
            return JsonResponse(
                {"message": f"Key {str(e)} not found in request body!"},
                status=400,
            )

        try:
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            if automobile.sold:
                return JsonResponse(
                    {"message": "This car has already been sold!"},
                    status=400,
                )
            salesperson = Salesperson.objects.get(employee_id=salesperson_id)
            customer = Customer.objects.get(id=customer_id)
        except ObjectDoesNotExist as e:
            return JsonResponse(
                {"message": f"{str(e)} does not exist in database!"},
                status=400,
            )

        try:
            with transaction.atomic():
                sale = Sale.objects.create(
                    automobile=automobile,
                    salesperson=salesperson,
                    customer=customer,
                    price=price
                )
                automobile.sold = True
                automobile.save()
        except Exception as e:
            return JsonResponse(
                {"message": f"Error while creating Sale object: {str(e)}"},
                status=400,
            )

        sale_dict = SaleDetailEncoder().default(sale)
        return JsonResponse(sale_dict, encoder=SaleDetailEncoder, safe=False)



@require_http_methods(["GET", "DELETE", "PUT"])
def show_sale(request, pk):
    try:
        sale = Sale.objects.get(id=pk)
    except Sale.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid sale id!"},
            status=400,
        )

    if request.method == "GET":
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        sale.delete()
        return JsonResponse({"deleted": True})
    elif request.method == "PUT":
        content = json.loads(request.body)
        for key, value in content.items():
            if hasattr(sale, key):
                try:
                    setattr(sale, key, value)
                except FieldError as e:
                    return JsonResponse(
                        {"message": f"Error in field assignment: {str(e)}"},
                        status=400,
                    )
        sale.save()
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False,
        )


#GET: filters only GET requests
#grabs all AutomobileVO objects
#turns them to a dict/json response using the encoder
#returns JSON containing the lsit of automobiles
@require_http_methods(['GET'])
def list_automobilevo(request):
    automobiles = AutomobileVO.objects.all()
    return JsonResponse(
        {'automobiles': list(automobiles.values())},
        encoder=AutomobileVODetailEncoder,
    )
#GET:  tries to retrieve a list of sales made by a specific Salesperson, identified by employee_id
# if person exists, fetch all sales associated with the sales person from the database
# serializes each sale obj using 'SaleListEncoder' and creates a lsit of JSON objs representing the sales.
# returns JSON response w/ list of sales amde by the unique salesperson
@require_http_methods(["GET"])
def salesperson_sales(request, employee_id):
    salesperson = get_object_or_404(Salesperson, employee_id=employee_id)
    sales = Sale.objects.filter(salesperson=salesperson)
    sales_list = [SaleListEncoder().default(sale) for sale in sales]
    return JsonResponse({"sales": sales_list}, safe=False)
